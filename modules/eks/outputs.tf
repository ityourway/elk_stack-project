output "endpoint" {
  value = aws_eks_cluster.gideonaslab.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.gideonaslab.certificate_authority[0].data
}
output "cluster_id" {
  value = aws_eks_cluster.gideonaslab.id
}
output "cluster_endpoint" {
  value = aws_eks_cluster.gideonaslab.endpoint
}
output "cluster_name" {
  value = aws_eks_cluster.gideonaslab.name
}
