## Elasticsearch Logstash Kibana Project

A comprehensive journey into log analysis as this repo you through the intricacies of deploying the ELK (Elasticsearch, Logstash, Kibana) stack on Amazon EKS (Elastic Kubernetes Service) using Terraform and Github Actions and  covers every critical aspect, ensuring you gain a thorough understanding of the process.

